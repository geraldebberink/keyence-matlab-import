function [X,Y,Z] = ReadKeyenceData(obj)
%
% readkeyenecedata.m opens and processes a csv file that is exported
% using the keyence vk9700 confocal microscope using the following
% settings:
%
% File - Output CSV file
if nargin ~= 1
    [ obj , ~, ~ ]=uigetfile('*.csv','Select Keyence csv file','standard.csv');
end

fid = fopen( obj );
Z = textscan(fid,'%f','Delimiter',',','Whitespace',' "\b\t','Headerlines',49);
fclose(fid);
fid = fopen( obj );
for i=1:48
    temp = fgetl(fid);
    switch i 
        case 40
            [ ~ , pixelSize, ~ ] = strread(temp,'%s%f%s','whitespace',' "\b\t','delimiter',',');
        case 41
            [ ~ , dataHeight, ~ ] = strread(temp,'%s%f%s','whitespace',' "\b\t','delimiter',',');
        case 47
            [ imageWidth , imageHeight , ~ ] = strread(temp,'%u%u%s','whitespace',' "\b\t','delimiter',',');
        otherwise
    end
end
      
X = linspace(0,imageWidth*pixelSize, imageWidth)/1000000;
Y = linspace(0,imageHeight*pixelSize, imageHeight)/1000000;
Z = cell2mat(Z);
Z = Z * dataHeight/1000000;
Z = reshape(Z,imageWidth,imageHeight)';
figure;subplot(2,1,1);surf(X,Y,Z);shading interp; axis tight;axis off;
subplot(2,1,2);surf(X,Y,Z);shading interp; axis tight;axis off;colorbar;view(2)






    