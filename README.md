# README #

This is a matlab script to import data of the Keyence 9700 laser scanning confocal microscope from the .asc file to into a matlab matrix.


### How do I get set up? ###

Move the script to a folder where it can be found.

Export the measurement as a .asc file.

### Contribution guidelines ###


* Check for open issues or open a fresh issue to start a discussion around a feature idea or a bug. There is a Contributor Friendly tag for issues that should be ideal for people who are not very familiar with the codebase yet.
*  Fork the rtc repository on bitbucket to start making your changes.
*  Write a test which shows that the bug was fixed or that the feature works as expected.
*  Send a pull request and bug the maintainer until it gets merged and published. :)
*  Make sure to add yourself to doc/contributors.md
